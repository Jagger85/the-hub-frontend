import { Box } from '@mui/material'
import React from 'react'
import { useEffect, useState } from 'react'
import { apiCalls } from '../../utils/apicalls'
import { Typography } from '@mui/material'
function UosPrice() {

    const [uosPrice , updateUosPrice] = useState('');

    useEffect(()=>{
    apiCalls.getUosPrice().then(price => updateUosPrice(price))
    },[])

  return (

    <Box>
        <Typography variant='h7light'>
        Uos price: {uosPrice}$
        </Typography>
    </Box>
  )
}

export default UosPrice