import React from 'react'
function Card() {
  const stl = {
    container: {
      padding: '4px',
      borderRadius: '10px',
      backgroundColor: '#423F4D',
      border: '0.2em solid #26242A',
      outline: '0.1em solid #605C64',
      outlineOffset: '-2px',
      boxShadow: '0px 2px 9px 1px rgba(0,0,0,0.75)',
    },
    image: {
      width: '200px',
      borderRadius: '5px',
      border: '0.05em solid #605C64',
    },
    content: {
      height: '7em',
    },
    chip: {
      width: '25px',
      height: '25px',
      rotate:'45deg',
      backgroundColor: '#423F4D',
      border: '0.1em solid #605C64',
      borderRadius: '3px',
      right: '0px',
      left: '0px',
      margin:'auto',
      position: 'relative',
      bottom: '20px'
    },
    price: {
      borderRadius: '5px',
      backgroundColor: '#896AE2',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '2em',
      color: 'white',
    },
  }

  return (
    <div className='container' style={stl.container}>
      <div style={stl.imageContainer}>
      <img src='src/assets/uniq.png' style={stl.image}/>
      <div style={stl.chip}/>
      </div>
      <div className='content' style={stl.content}>
        content
      </div>
      <div className='price' style={stl.price}>
        price
      </div>
    </div>
  )
}

export default Card